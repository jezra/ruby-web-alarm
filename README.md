Set Up
------

copy config.yaml.tmp to config.yaml
edit config.yaml to set the port you wish Ruby Web Alarm to use


Running Ruby Web Alarm
----------------------

run rwa.rb
point your browser to the HOST_NAME:PORT of your Ruby Web Alarm server

Jezra's example: my Ruby Web Alarm is running on a machine named wallbone 
and is configured to use port 8000, so I access the app by directing my
browser to  "http://wallbone:8000" 

Creating "Alarms"
-----------------
Alarms used by Ruby Web Alarm are nothing more than executable scripts
that are placed in the Alarms directory

