#!/usr/bin/env ruby

####
# Copyright 2012 Jezra

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
####

require 'webrick'
require 'json'
require 'yaml'

@LOCAL_DIR = File.expand_path( File.dirname(__FILE__) )
@ALARM_DIR = File.join(@LOCAL_DIR,"alarms")
@PUBLIC_DIR = File.join(@LOCAL_DIR,"public")

@alarm_queue=[]

def is_valid_alarm( alarm )
	alarm_list.include? alarm
end

def alarm_list
	entries = Dir.entries( @ALARM_DIR ).select {|m| m unless m.start_with?"."  }
	entries.sort
end

def load_config
	@config = YAML.load_file(File.join(@LOCAL_DIR,"config.yaml"))
	p @config.inspect
end

def alarms( req, res)
	#return a list of the alarms in the alarms dir
	entries = alarm_list
	res['Content-Type'] = 'text/json'
  res.body = JSON.dump(entries)
end

def alarms_pending( req, res)
	#return a list of the alarms pending for execution
	res['Content-Type'] = 'text/json'
  res.body = JSON.dump(@alarm_queue)
end

def current_time( req, res)
	now = Time.now()
	time = {:time=>now.strftime("%l:%M"), :meridiem=>now.strftime("%p"), :now=>now.strftime("%H:%M")}
	res['Content-Type'] = 'text/json'
	res.body = JSON.dump(time)
end

def set_alarm(req, res)
	res['Content-Type'] = 'text/plain'
		
	unless req.request_method == "POST" 
		res.status = 400
		res.body = "bad request"
	else
		now = Time.now()
		time = Time.parse(req.query['time'])
		alarms = JSON.parse(req.query['alarm[]'])
		if time < now
		 time += (24*60*60)
		end
		alarms.each do |alarm|
			if is_valid_alarm( alarm )
				#add the alarm to the queue
				@alarm_queue << {'time'=>time,'alarm'=>alarm}
				# sort the alarm queue
				@alarm_queue=@alarm_queue.sort_by {|a| a['time']}
				res.body += "run #{alarm} at #{time}\n"
			else
				res.status = 400
				res.body += "invalid alarm : #{alarm}"
			end
		end
		
	end
end

def run_alarm(req, res)
	res['Content-Type'] = 'text/plain'
		
	unless req.request_method == "POST" 
		res.status = 400
		res.body = "bad request"
	else
		res.body = ""
		alarms = JSON.parse(req.query['alarm[]'])
		alarms.each do |alarm|
			if is_valid_alarm alarm
				alarm_to_run = File.join(@ALARM_DIR, alarm)
				system(alarm_to_run+" &")
				res.body += "running alarm : #{alarm}\n"
			else
				res.status = 400
				res.body += "invalid alarm: #{alarm}\n"
			end
		end
	end
end

def cancel_alarm(req, res)
	res['Content-Type'] = 'text/plain'
		
	unless req.request_method == "POST" 
		res.status = 400
		res.body = "bad request"
	else
		time = Time.parse(req.query['time'])
		alarm = req.query['alarm']
		#add the alarm to the queue
		@alarm_queue.each do |a|
			if a['time'] == time and a['alarm'] == alarm
				@alarm_queue.delete(a)
				break
			end
		end
		res.body = "alarm cancelled"
	end
end

def run_alarm_processor 
	Thread.new do
		while true
			sleep 30
			now = Time.now()
			@alarm_queue.each do |a|
				if a['time'] <= now 
					#run the alarm
					alarm = File.join(@ALARM_DIR, a['alarm'])
					p alarm
					system(alarm+" &")
					#remove this alarm from the queue
					@alarm_queue.delete( a )
				end
			end
		end
	end
end

# Initialize our WEBrick server
if $0 == __FILE__ 
	#load the config 
	load_config
	#get the server config from config
	sc = @config['Server']
	p sc.inspect
	server = WEBrick::HTTPServer.new({:Port=>sc['Port'].to_i,:DocumentRoot=>@PUBLIC_DIR})
	#what urls do we need to mount?

	server.mount_proc('/alarms') do |req, resp| 
		alarms( req, resp )
	end
	
	server.mount_proc('/alarms_pending') do |req, resp| 
		alarms_pending( req, resp )
	end
	
	server.mount_proc('/set_alarm') do |req, resp| 
		set_alarm( req, resp )
	end
	
	server.mount_proc('/run_alarm') do |req, resp| 
		run_alarm( req, resp )
	end
	
	server.mount_proc('/cancel_alarm') do |req, resp| 
		cancel_alarm( req, resp )
	end
	
	server.mount_proc('/current_time') do |req, resp| 
		current_time( req, resp )
	end
	
	trap "INT" do 
		server.shutdown 
	end
	
	#run the alarm processor
	run_alarm_processor
	
	#start the server
	server.start
	
end
