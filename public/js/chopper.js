/* chopper v 0.0.2 */
/* chopper variables */
current_page = undefined;

$(function(){
	//hide all of the pages
  $(".page").hide();
  switch_to_page(start_page);
	//set up the page navs
  $(".page_nav").click ( function() {
		var target = $(this).attr("target");
		switch_to_page(target, this);
	});
	//set up the radio button groups
	set_up_radio_button_groups();
});

function set_up_radio_button_groups() {
	var rbgs = $(".radio_button_group");
	for( var i=0 ; i < rbgs.length ; i++ ) {
    var group_id = $(rbgs[i]).attr("id");
		set_up_radio_button_group( group_id );
	}
}

function set_up_radio_button_group(group_id) {
	var rbs = $("#"+group_id+" button");
	for( var i=0 ; i < rbs.length ; i++ ) {
		$(rbs[i]).click( click_radio_button ); 
	}
}

function click_radio_button() {
	var clicked_button = this;
	//does the thing have a callback?
	if ($._data(clicked_button, "events").radio_callback != undefined) {
		$(clicked_button).trigger('radio_callback');
	}
	var name = $(clicked_button).attr('name');
	//disable clicked_button button
	radio_button_select(clicked_button);
	//enable all other buttons with clicked_button name
	var buttons = $("button[name='"+name+"']");
	for(var i = 0; i < buttons.length; i++ ) {
		var button = buttons[i];
		if (button != clicked_button) {
			radio_button_enable(button);
		}
	}
}

//button control stuff
function button_disable( button_id ){
	$(button_id).addClass("faux_button_disabled");
	$(button_id).attr("disabled", true);
}

function button_enable( button_id ) {
	$(button_id).removeClass("faux_button_disabled");
	$(button_id).attr("disabled", false);
}

function button_disabled( button_id ) {
	return $(button_id).hasClass( "faux_button_disabled" );
}

function button_select( element ) {
	$(element).addClass("faux_button_selected");
}

function button_deselect( element ) {
	$(element).removeClass("faux_button_selected");
}

function radio_select( element ) {
	$(element).addClass("radio_selected");
}

function radio_deselect( element ) {
	$(element).removeClass("radio_selected");
}

function radio_button_select( element ) {
	$(element).attr("disabled", true);
	$(element).addClass("radio_selected");
}

function radio_button_enable( element ) {
	$(element).attr("disabled", false);
	$(element).removeClass("radio_selected");
}

//page navigation
function switch_to_page( page, source ) {	
	if (source != undefined ) {
		if (button_disabled( $(source) ) ) {
			return;
		}
	}
	//hide the current page
	if (current_page != undefined) {
		$("#"+current_page).hide();
	}
	$("#"+page).show();
	current_page = page;
	
	//the page was switched?
	try {
		page_switched(page);
	}catch(error) {
		alert(error);
	}
}

//some basic logging
function log(string) {
	try {
		console.log(string);
	} catch(error) {
		//there may not be a page switch callback
	}
}
