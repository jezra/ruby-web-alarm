//global app variables
start_page = "PendingAlarms";

dayNames = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
monthNames = Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")



$(function(){
	//set some global variables
	current_selected_pending_timed_alarm = undefined;
	current_selected_meridiem = undefined;
	set_up_buttons();
	set_up_radios();
	//create the time input
	create_time_input();
	get_current_time();
});

function page_switched(page) {
	get_pending_alarms();
	switch(page) {
		case "PendingAlarms" :
			break;
		case "NewAlarm" :
			get_alarms();
			break;
	}
}

function create_time_input(){
	//create the time input using plain old shitty javascript
	time_input = document.createElement("input");
	time_input.setAttribute("type", "time");
	time_input.setAttribute("id", "input_time");
	//append the time_input the the wrapper
	$("#time_wrapper").prepend(time_input);
	if (time_input.type!="text") {
		has_time_input = true;
		//hide the meridiem stuff
		$("#meridiem_radio_block").hide();
		// hide the time changer do-hickeys
		$("#time_changer_block").hide();
	} else {
		has_time_input = false;
	}
}

function set_up_buttons() {
	$("#button_hour_up").bind('click', click_hour_up );
	$("#button_hour_down").bind('click', click_hour_down );
	$("#button_minute_up").bind('click', click_minute_up );
	$("#button_minute_down").bind('click', click_minute_down );
	$("#button_new_alarm_submit").bind('click', click_new_alarm_submit);
	$("#button_pending_alarm_cancel").bind('click', click_pending_alarm_cancel);
	$("#button_pending_alarm_cancel_all").bind('click', click_pending_alarm_cancel_all);
	$("#button_alarm_run_now").bind('click', click_alarm_run_now);
	//set up the page navs
  $(".page_nav").click ( function() {
		var target = $(this).attr("target");
		switch_to_page(target, this);
	});
}

function set_up_radios() {
	//technically, these are buttons ;)
  $("button[name=meridiem]").bind('radio_callback', change_meridiem);
}

function reset_alarms() {
	//deselect all options
	var str = "";
	$( "#select_alarm option:selected" ).each(function() {
		$( this ).prop("selected", false);
	});
}

function click_hour_up() {
	alarm_hour += 1;
	if (alarm_hour > 12 ) {
		alarm_hour = 1;
	}
	update_input_time();
}

function click_hour_down() {
	alarm_hour -= 1;
	if (alarm_hour <=0 ) {
		alarm_hour = 12;
	}
	update_input_time();
}
function click_minute_up() {
	alarm_minute += 1;
	if (alarm_minute > 59 ) {
		alarm_minute = 1;
	}
	update_input_time();
}

function click_minute_down() {
	alarm_minute -= 1;
	if (alarm_minute <=0 ) {
		alarm_minute = 59;
	}
	update_input_time();
}

function click_new_alarm_submit() {
	var alarm = $("#select_alarm").val();
	if (has_time_input) {
		var mer = "PM";
		var tval = $("#input_time").val();
		var hour = parseInt(tval.split(":")[0],10);
		var min = tval.split(":")[1];
		if (hour < 13) {
			mer = "AM";
		} else {
			hour = hour -12;
		}
		var time = hour+":"+min+mer;
	} else {
		var time = $("#input_time").val() + current_selected_meridiem;
	}
	var result = confirm("Set "+alarm+" for "+time+"?");
	if( !result ) {
		return;
	}

	var url = "/set_alarm";
  $.ajax({
    url: url,
    type: "POST",
    data: { 'time': time, 'alarm[]': JSON.stringify(alarm) },
    dataType: 'text',

    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//update the pending alarms
			get_pending_alarms();
			//reset the alarms
			reset_alarms();
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : set_alarm : "+type);
    }
  });
}

function click_alarm_run_now() {
	var alarm = $("#select_alarm").val();
	var result = confirm("Run "+alarm+" now?");
	if( !result ) {
		return;
	}

	var url = "/run_alarm";
  $.ajax({
    url: url,
    type: "POST",
    data: { 'alarm[]': JSON.stringify(alarm)},
    dataType: 'text',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//reset the alarms
			reset_alarms();
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : run_alarm : "+type);
    }
  });
}

function update_input_time() {
	var minute = (alarm_minute < 10) ? "0"+alarm_minute : alarm_minute;
	var time_text = alarm_hour+":"+minute;
	$("#input_time").val(time_text);
}

function process_current_time_data( data ) {
	if (has_time_input) {
		$("#input_time").val(data['now']);
	} else {
		var time_split = data['time'].split(":");
		alarm_hour = parseInt(time_split[0],10);
		alarm_minute = parseInt(time_split[1],10);
		meridiem = data['meridiem'];
		$("#meridiem_radio_block button[value="+meridiem+"]").click()
		update_input_time();
	}
}

function get_pending_alarms() {
	var url = "/alarms_pending";
  $.ajax({
    url: url,
    dataType: 'json',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_pending_alarm_data( body );
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :alarms: "+type);
    }
  });
}

function get_alarms() {
	var url = "/alarms";
  $.ajax({
    url: url,
    dataType: 'json',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_alarm_data( body );
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :alarms: "+type);
    }
  });
}

function get_current_time() {
	var url = "/current_time";
  $.ajax({
    url: url,
    dataType: 'json',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_current_time_data( body );
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error :alarms: "+type);
    }
  });
}

function process_alarm_data(data) {
	var select_alarm = $("#select_alarm");
	//clear the alarm select
	select_alarm.empty();
	for (var index in data) {
		var alarm = data[index];
		var option = $("<option>");
		option.val(alarm);
		option.html(alarm);
		select_alarm.append(option);
	}
}

function change_meridiem() {
	current_selected_meridiem = $(this).val();
}

function change_pending_alarm( ) {
	//enable the cancel button
	button_enable("#button_pending_alarm_cancel");
	//have we switched buttons?
	if (current_selected_pending_timed_alarm != this ) {
		current_selected_pending_time = $(this).attr("time");
		current_selected_pending_alarm = $(this).attr("alarm");
		//highlight some div
		if (current_selected_pending_timed_alarm!=undefined) {
			button_deselect( $(current_selected_pending_timed_alarm) );
		}
		current_selected_pending_timed_alarm = this;
		button_select( $(current_selected_pending_timed_alarm) );
	}
}

function click_pending_alarm_cancel( ) {
	//is the button clickable?
	if (button_disabled("#button_pending_alarm_cancel") ){
		return;
	}
	//confirm the cancel
	var result = confirm("Really cancel this alarm?");
	if( !result ) {
		return;
	}
	cancel_alarm(current_selected_pending_time, current_selected_pending_alarm );
	get_pending_alarms();
}


function cancel_alarm(time, alarm){
	var url = "/cancel_alarm";
  $.ajax({
    url: url,
    type: "POST",
    data: {
			time: time,
			alarm: alarm
		},
    dataType: 'text',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//do nothing!
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log(type+" : cancel_alarm : "+xhr);
    }
  });

}
function click_pending_alarm_cancel_all( ) {
	//is the button clickable?
	if (button_disabled("#button_pending_alarm_cancel_all") ){
		return;
	}
	//confirm the cancel
	var result = confirm("Really cancel all alarms?");
	if( !result ) {
		return;
	}
	//loop through the pending alarms, and cancel them
	$("#pending_alarms_list").children("button").each(function(i,e) {
		var object = $(e);
		var t = object.attr("time");
		var a = object.attr("alarm");
		cancel_alarm(t,a);
	});
	get_pending_alarms();
}

function process_pending_alarm_data(data) {
	//disable the cancel button
	button_disable("#button_pending_alarm_cancel");
	var pending_alarms_list = $("#pending_alarms_list");
	var next_pending_alarm = $("#next_pending_alarm");
	//clear the alarm select
	pending_alarms_list.empty();
	//clear the next_pending alarm
	next_pending_alarm.empty();

	if (data.length > 0 ) {
		for (var index in data) {
			var alarm = data[index]['alarm'];
			var time = data[index]['time'];
			var btn = $("<button class='faux_button selectable_row' name='pending_alarm'>");
			btn.attr("time",time);
			btn.attr("alarm",alarm);
			var str = nice_time(time)+" : "+alarm;
			btn.append(str);
			if (index == 0) {
				next_pending_alarm.text(str);
			}
			pending_alarms_list.append(btn);
			btn.bind('radio_callback', change_pending_alarm );
		}
		set_up_radio_button_group("pending_alarms_list");
		//enable the cancel all button
		button_enable("#button_pending_alarm_cancel_all");
	} else {
		pending_alarms_list.html("<tr><td>No Alarms Are Set</td></tr>");
		button_disable("#button_pending_alarm_cancel_all");
	}
}

function nice_time( time ) {
	var time_parts = time.split(" ");
	var ymd_parts = time_parts[0].split("-");
	var year = parseInt(ymd_parts[0],10);
	var month = parseInt(ymd_parts[1],10);
	var day = parseInt(ymd_parts[2],10);
	var hms_parts = time_parts[1].split(":");
	var hour = parseInt(hms_parts[0],10);
	var minute = parseInt(hms_parts[1],10);
	var date = new Date(year, month-1, day, hour, minute, 0, 0);
	//build up a new string
	var hour = date.getHours();
	if (!hour) {
		return time;
	}
	var meridiem = "AM";
	if (hour > 12) {
		hour -= 12;
		meridiem = "PM";
	}
	var date_string = dayNames[ date.getDay() ];
	date_string +=", "+monthNames[date.getMonth()];
	date_string +=" "+date.getDate();
	if (minute < 10) {
		minute = "0"+minute;
	}
	date_string +=" "+hour+":"+minute;
	date_string +=" "+meridiem;
	return date_string;
}
